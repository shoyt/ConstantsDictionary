# Constants Dictionary
Constants Dictionary is an iOS application used for storing and referencing physical constants in science. The application comes pre-loaded with common physics constants and the user has the ability to add custom constants. 

This was my main project for trying to learn iOS development and has not been updated since 2015.

Dec 29, 2021: Migrated from Github
